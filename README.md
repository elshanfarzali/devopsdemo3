# DevOpsDemo3
## Deploying Spring PetClinic Sample Application on AWS using Docker and Terraform

- **Subtask I - Create Terraform scripts for AWS infrasrtucture:**
  - For running `terraform apply` command, we create `.tf` files to set up VPC, security groups, EC2, RDC, variables and so on. Firstly we mention variables in [variables.tf](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/blob/main/k8-terraform/variables.tf).
  - To create EKS on AWS we set recources on [eks.tf](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/blob/main/k8-terraform/eks.tf). As well as we create other files for provisioning.
  - On [output.tf](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/blob/main/k8-terraform/output.tf) we configure what should be shown.

    * aws_vpc - Provides a Virtual Private Network for AWS instance;
    * aws_subnet - I created a public subnet for EC2 instance;
    * aws_security_group - For EC2 instance to set connection policies;
    * aws_internet_gateway - To make EC2 instance have internet access;
    * aws_route_table - For being routed packets to the internet gateway;
    * aws_route_table_association - It shows packets in which subnets will be routed
    * aws_network_interface - It create network interface card for EC2;
    * EKS - AWS Kubernetes clusetr;

  - To deploy Apache Web Server on AWS EKS and K8 dashboard we set recources on [kubernetes](https://gitlab.com/elshanfarzali/k8-elasticsearch/-/tree/main/kubernetes).


- **Subtask II - Gitlab:**
  - Before running pipeline we should add AWS and some other variables (for more information [gitlab-variables](https://docs.gitlab.com/ee/ci/variables/)) and configure shell gitlab-runner on local:
      ```
      gitlab-runner register -n \
      --url ${gitlab_registry} \
      --registration-token "${gitlab_token}" \
      --executor shell \
      --description "your description"
      ```
  - I created 6 jobs and one stage for each job:

    * init - It initializes terraform and installs required plugins;
    * validate - Validate that terraform config is ok;
    * plan - It creates execution plan for next stage;
    * apply - Apply terraform based on execution plan;
    * deploy - Deploy kuberenetes manifest files on EKS;
    * destroy - This stage running manually to destroy config if there is need;
