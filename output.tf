#Show Private and public IP
output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.petclinic.public_ip
}
output "instance_private_ip" {
  description = "Private IP address of the EC2 instance"
  value       = aws_instance.petclinic.private_ip
}
output "db-address" {
    description = "DB instance endpoint"
    value = aws_db_instance.petclinic-db.address
}