#Creating RDS
resource "aws_db_parameter_group" "petclinic_rds" {
  name   = "rds-pg"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_subnet_group" "main" {
  name       = "main"
  subnet_ids = [ aws_subnet.private1.id, aws_subnet.private2.id ]

  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_security_group" "rds-sg" {
  name        = "rds-security-group"
  description = "allow inbound access to the database"
  vpc_id      = aws_vpc.demo3-vpc.id 

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = [ var.vpc_cidr ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [ var.vpc_cidr ]
  }
}

resource "aws_subnet" "private1" {
  vpc_id     = aws_vpc.demo3-vpc.id 
  cidr_block = var.vpc_cidr_private[0]
  availability_zone = var.availability_zone_names[0]

  tags = {
    Name = "RDS-net-private1"
  }
}

resource "aws_subnet" "private2" {
  vpc_id     = aws_vpc.demo3-vpc.id 
  cidr_block = var.vpc_cidr_private[1]
  availability_zone = var.availability_zone_names[1]

  tags = {
    Name = "RDS-net-private2"
  }
}
resource "aws_db_instance" "petclinic-db" {
  allocated_storage    = 100
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t2.micro"
  identifier           = var.db_name
  name                 = var.db_name
  username             = var.db_user
  password             = var.db_pass
  parameter_group_name = aws_db_parameter_group.petclinic_rds.id
  db_subnet_group_name = aws_db_subnet_group.main.id
  vpc_security_group_ids = [ aws_security_group.rds-sg.id ]
  publicly_accessible  = false
  skip_final_snapshot  = true
  multi_az             = false
}