provider "aws" {
    shared_credentials_file = "$HOME/.aws/credentials"
    region                  = "us-east-2"
}
#Create VPC
resource "aws_vpc" "demo3-vpc" {
    cidr_block = var.vpc_cidr
    tags = {
      "Name" = var.vpc_name
    }
}

#Create Internet Gateway
resource "aws_internet_gateway" "demo3-gateway" {
    vpc_id = aws_vpc.demo3-vpc.id   
    tags = {
    "Name" = "${var.vpc_name}-GW"
    }   
}

#Create Subnet
resource "aws_subnet" "demo3-subnet" {
  vpc_id = aws_vpc.demo3-vpc.id
  cidr_block = var.vpc_cidr_public
  availability_zone = var.availability_zone_names[0]
  map_public_ip_on_launch = true
  tags = {
    "Name" = "${var.vpc_name}-sub"
  }
}
resource "aws_route_table" "terra-route" {
  vpc_id = aws_vpc.demo3-vpc.id 

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo3-gateway.id
  }
  tags = {
    Name = "${var.vpc_name}-iba-route"
  }
}
resource "aws_route_table_association" "iba" {
  subnet_id      = aws_subnet.demo3-subnet.id
  route_table_id = aws_route_table.terra-route.id
}
#Create Security Group
resource "aws_security_group" "allow_web_demo3" {
  name = "allow_web_traffic"
  description = "Allow for traffic"
  vpc_id = aws_vpc.demo3-vpc.id
  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "PETCLINIC"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  # ingress {
  #   description      = "HTTPS"
  #   from_port        = 443
  #   to_port          = 443
  #   protocol         = "tcp"
  #   cidr_blocks      = ["0.0.0.0/0"]
  # }
  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.vpc_name}-allow_web_demo3"
  }
}
#Create network interface
resource "aws_network_interface" "demo3-interface" {
  subnet_id       = aws_subnet.demo3-subnet.id
  security_groups = [aws_security_group.allow_web_demo3.id]
  tags = {
    Name = "demo3-interface"
  }
}
#Create AWS Linux Machine
resource "aws_instance" "petclinic" {
  ami = var.ami_id
  instance_type = var.instance_type
  availability_zone = var.availability_zone_names[0]
  key_name = "id_rsa"
  depends_on = [
      aws_db_instance.petclinic-db,
      aws_vpc.demo3-vpc,
      aws_internet_gateway.demo3-gateway,
  ]
  network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.demo3-interface.id
  }  
#  provisioner "file" {
#    source      = "/home/elshan/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar"
#    destination = "/home/ec2-user/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar"
#    connection {
#        type     = "ssh"
#        user     = "ec2-user"
#        password = ""
#        private_key = "${file("~/.ssh/id_rsa.pem")}"
#        host = aws_instance.petclinic.public_ip
#    }
#  }
  user_data = <<EOF
    #!/bin/bash
    export db_address="${aws_db_instance.petclinic-db.address}"
    sudo yum update -y
    sudo amazon-linux-extras install docker -y
    sudo yum install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
    sudo docker login  registry.gitlab.com -u ${var.git_user} -p ${var.token}
    sudo docker run -dit -p 80:8080 --name=petclinic-app --restart=on-failure:10 -e MYSQL_USER=${var.db_user} -e MYSQL_PASS=${var.db_pass} -e MYSQL_URL=jdbc:mysql://$db-address:${var.db_port}/${var.db_name} ${var.ci_registry}
	  EOF
  tags = {
    "Name" = "AWS-Linux-Petlinic"
  }
}