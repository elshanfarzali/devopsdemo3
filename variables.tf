variable "aws_region" {
    default = "us-east-2"
}

variable "ami_id" {
    default = "ami-077e31c4939f6a2f3"
}

variable "instance_type" {
  default = "t2.micro"
}
variable "availability_zone_names" {
  type    = list(string)
  default = ["us-east-2a", "us-east-2b"]
}
# variable "availability_zone" {
#   default = "us-east-2a"
# }

variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR for VPC"
}
variable "vpc_cidr_public" {
  type        = string
  default     = "10.0.0.0/24"
  description = "PUBLIC CIDR for VPC"
}
# variable "vpc_cidr_private1" {
#   type        = string
#   default     = "10.0.2.0/24"
#   description = "PRIVATE1 CIDR for VPC"
# }
variable "vpc_cidr_private" {
  type    = list(string)
  default = ["10.0.2.0/24", "10.0.3.0/24"]
  description = "PRIVATE1 CIDR for VPC"
}
variable "vpc_name" {
  type        = string
  default     = "main"
  description = ""
}
variable "db_user" {
  type = string
}
variable "db_pass" {
  type = string
}
variable "db_name" {
  type = string
  default = "petclinicdb"
}
variable "ci_registry" {
  type = string
  default = "registry.gitlab.com/elshanfarzali/devopsdemo3:latest"
}
variable "db_port" {
  type = string
  default = "3306"
}
variable "token" {
  type = string
}
variable "git_user" {
  type = string
}